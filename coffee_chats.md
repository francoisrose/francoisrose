# Coffee chats

Keeping track of the [coffee chats](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats) I've had so far at GitLab.

Thanks to [Carrie Kroutil](https://gitlab.com/ckroutil/ckroutil/-/blob/main/coffee-chats.md) for this fantastic idea!

Legend
- 🏳️ Location
- ☕️ Regular coffee chat
- 🍩 Donut bot paired

| Last chat | Person | Role | Count |
|-----------|--------|------|-------|
| `2024-02-29` | 🇳🇱 [Manoj Memana Jayakumar](https://gitlab.com/manojmj)            | Senior Backend Engineer, Data Stores:Tenant Scale | ☕️ |
| `2024-02-28` | 🇩🇪 [Paulina Sędłak-Jakubowska](https://gitlab.com/psjakubowska)    | Senior Frontend Engineer, Create:Source Code | ☕️🍩🍩🍩🍩☕️ |
| `2024-02-26` | 🇳🇱 [Laura Ionel](https://gitlab.com/lauraionel)                    | Senior Backend Engineer, Create:Editor Extensions | ☕️ |
| `2024-02-26` | 🇨🇿 [Tomas Vik](https://gitlab.com/viktomas)                        | Staff Fullstack Engineer, Create:Editor Extensions | ☕️ |
| `2024-02-26` | 🇮🇳 [Rajendra Kadam](https://gitlab.com/rkadam3)                    | Senior Backend Engineer, Verify:Pipeline Authoring | ☕️☕️☕️ |
| `2024-02-22` | 🇨🇭 [David Dieulivol](https://gitlab.com/ddieulivol)                | Sr. Backend Engineer, Engineering Productivity | ☕️☕️☕️☕️☕️☕️ |
| `2024-02-21` | 🇺🇸 [John Cai](https://gitlab.com/jcaigitlab)                       | Engineering Manager, Gitaly:Git | ☕️ |
| `2024-02-19` | 🇩🇪 [Vitali Tatarintev](https://gitlab.com/ck3g)                    | Senior Backend Engineer, Create:Code Creation | ☕️☕️☕️ |
| `2024-02-15` | 🇮🇹 [Nicolò Mezzopera](https://gitlab.com/nmezzopera)               | Engineering Manager, Deploy:Environments | ☕️☕️☕️☕️ |
| `2024-02-15` | 🇳🇱 [Nick Ilieskou](https://gitlab.com/nilieskou)                   | Senior Backend Engineer, Secure: Composition Analysis | ☕️☕️☕️ |
| `2024-02-12` | 🇩🇪 [Peter Leitzen](https://gitlab.com/splattael)                   | Staff Backend Engineer, Engineering Productivity | ☕️🍩🍩🍩☕️☕️☕️ |
| `2024-02-08` | 🇩🇪 [Marc Shaw](https://gitlab.com/marc_shaw)                       | Senior Backend Engineer, Create:Code Review BE | ☕️ |
| `2024-02-07` | 🇺🇸 [Kerri Miller](https://gitlab.com/kerrizor)                     | Staff Backend Engineer, Create:Code Review BE | ☕️ |
| `2024-02-07` | 🇺🇸 [Gary Holtz](https://gitlab.com/garyh)                          | Backend Engineer, Create:Code Review BE | ☕️ |
| `2024-02-06` | 🇵🇭 [Patrick Bajao](https://gitlab.com/patrickbajao)                | Staff Backend Engineer, Create:Code Review BE | ☕️ |
| `2024-02-06` | 🇦🇺 [Sincheol Kim](https://gitlab.com/dskim_gitlab)                 | Senior Backend Engineer, Create:Code Review BE | ☕️ |
| `2024-01-29` | 🇺🇸 [Crystal Poole](https://gitlab.com/crystalpoole)                | Senior Engineering Manager, Package | ☕️☕️☕️☕️☕️☕️ |
| `2024-01-29` | 🇺🇸 [Mark Nuzzo](https://gitlab.com/marknuzzo)                      | Engineering Manager, Verify:Pipeline Authoring | ☕️☕️☕️ |
| `2024-01-18` | 🇳🇱 [Francesca Gianfiglio](https://gitlab.com/fgianfiglio)          | Senior BDR, EMEA MM | 🍩 |
| `2024-01-02` | 🇺🇸 [Sam Goldstein](https://gitlab.com/sgoldstein)                  | Director of Engineering, Ops | ☕️☕️☕️☕️ |
| `2023-11-30` | 🇺🇸 [Dasha Adushkina](https://gitlab.com/dashaadu)                  | Sr. Product Manager, Create:Editor Extensions | ☕️ |
| `2023-11-29` | 🇨🇦 [Ade Adesanya](https://gitlab.com/adebayo_a)                    | Engineering Manager, Create:IDE | ☕️ |
| `2023-11-29` | 🇩🇪 [Katie Macoy](https://gitlab.com/katiemacoy)                    | Senior Product Designer, AI Framework | ☕️ |
| `2023-11-22` | 🇺🇸 [Mavryck Richardson](https://gitlab.com/kisha.mavryck)          | Engineering Manager, Create:Code Review BE | ☕️ |
| `2023-11-20` | 🇨🇭 [Sean Carroll](https://gitlab.com/sean_carroll)                 | Engineering Manager, Create:Source Code BE | ☕️ |
| `2023-11-03` | 🇦🇺 [Jay McCure](https://gitlab.com/jay_mccure)                     | Senior Software Engineer in Test, Create | ☕️ |
| `2023-10-25` | 🇺🇸 [Derek Ferguson](https://gitlab.com/derekferguson)              | Group Manager, Product - Create | ☕️ |
| `2023-10-10` | 🇳🇱 [David O'Regan](https://gitlab.com/oregand)                     | Engineering Manager, AI Framework | ☕️ |
| `2023-09-28` | 🇺🇸 [Amy Qualls](https://gitlab.com/aqualls)                        | Senior Technical Writer | ☕️ |
| `2023-09-28` | 🇮🇪 [Erran Carey](https://gitlab.com/erran)                         | Staff Incubation Engineer, Breach and Attack Simulation | ☕️ |
| `2023-09-11` | 🇦🇹 [Tim Zallmann](https://gitlab.com/timzallmann)                  | Senior Director of Engineering, Dev | ☕️ |
| `2023-09-08` | 🇨🇦 [Anthony Pham](https://gitlab.com/antpham)                      | Senior BDR, Enterprise | 🍩 |
| `2023-09-07` | 🇺🇸 [John Slaughter](https://gitlab.com/john-slaughter)             | Staff Backend Engineer, Create:Editor Extensions | ☕️ |
| `2023-08-30` | 🇺🇸 [Kai Armstrong](https://gitlab.com/phikai)                      | Sr. Product Manager, Create:Code Review | ☕️ |
| `2023-08-28` | 🇺🇸 [Matt Nohr](https://gitlab.com/mnohr)                           | Engineering Manager, Create:Code Creation/Code Review | ☕️ |
| `2023-08-28` | 🇺🇸 [Michelle Gill](https://gitlab.com/m_gill)                      | Director of Engineering, Manage | ☕️ |
| `2023-08-21` | 🇺🇸 [Cedric Stapleton](https://gitlab.com/c.stapleton)              | Manager, CSM AMER COM | 🍩 |
| `2023-08-09` | 🇩🇪 [Torsten Linz](https://gitlab.com/tlinz)                        | Senior Product Manager, AI Framework | ☕️ |
| `2023-08-09` | 🇺🇸 [Sacha Guyon](https://gitlab.com/sguyon)                        | Senior Product Manager, Observability | ☕️ |
| `2023-08-07` | 🇺🇸 [Wakae McLaurin](https://gitlab.com/wmclaurin)                  | Senior Professional Services Project Coordinator | 🍩 |
| `2023-08-03` | 🇺🇸 [Amelia Bauerly](https://gitlab.com/ameliabauerly)              | Senior Product Designer, Tenant Scale | ☕️🍩☕️ |
| `2023-07-20` | 🇳🇱 [Maina Ng'ang'a](https://gitlab.com/anganga)                    | Site Reliability Engineer | 🍩 |
| `2023-07-19` | 🇫🇷 [Pauline Lamon](https://gitlab.com/plamon1)                     | Sr. Major Account Executive | 🍩 |
| `2023-06-27` | 🇳🇱 [Remco Koedijk](https://gitlab.com/RemcoKoedijk)                | Strategic Account Leader (Benelux) | 🍩 |
| `2023-06-22` | 🇺🇸 [Sarah Yasonik](https://gitlab.com/syasonik)                    | Senior Backend Engineer, Monitor:Respond | ☕️☕️🍩🍩🍩 |
| `2023-06-19` | 🇺🇸 [Dave Peterson](https://gitlab.com/dpeterson1)                  | Staff Product Analyst | ☕️ |
| `2023-06-07` | 🇩🇪 [Marc Saleiko](https://gitlab.com/msaleiko)                     | Senior Incubation Engineer | ☕️☕️ |
| `2023-06-01` | 🇺🇸 [Alyson Sebby Turner](https://gitlab.com/sebbyturner)           | Business Development Representative | 🍩 |
| `2023-05-22` | 🇺🇸 [Nicholas Klick](https://gitlab.com/nicholasklick)              | Engineering Manager, Monitor:Observability | ☕️☕️ |
| `2023-05-17` | 🇨🇦 [Félix Veillette-Potvin](https://gitlab.com/fvpotvin)           | Application Security Engineer | ☕️ |
| `2023-05-16` | 🇳🇱 [Marta Kotek](https://gitlab.com/mkotek1)                       | Deal Desk Specialist | 🍩 |
| `2023-05-15` | 🇮🇳 [Ankit Panchal](https://gitlab.com/ankit.panchal)               | Senior Frontend Engineer, Analytics Instrumentation | ☕️☕️ |
| `2023-05-11` | 🇬🇧 [Daniele Rossetti](https://gitlab.com/drosse)                   | Senior Frontend Engineer, Monitor:Observability | ☕️☕️ |
| `2023-05-10` | 🇨🇭 [Max Orefice](https://gitlab.com/morefice)                      | Acting Engineering Manager, Verify:Pipeline Security | ☕️ |
| `2023-05-04` | 🇬🇧 [Lee Tickett](https://gitlab.com/leetickett-gitlab)             | Fullstack Engineer, Contributor Success | ☕️ |
| `2023-03-23` | 🇩🇪 [Matthias Käppler](https://gitlab.com/mkaeppler)                | Staff Engineer, Application Performance | ☕️☕️ |
| `2023-02-28` | 🇺🇸 [Blake Chalfant-Kero](https://gitlab.com/bchalfantkero)         | SMB Named Account Executive | 🍩 |
| `2023-02-23` | 🇩🇪 [Marcin Sędłak-Jakubowski](https://gitlab.com/msedlakjakubowski)| Technical Writer, Plan and Monitor | ☕️ |
| `2023-02-21` | 🇺🇸 [Jackie Porter](https://gitlab.com/jreporter)                   | Director, Product - Verify & Package | ☕️ |
| `2023-02-16` | 🇺🇸 [Darren Eastman](https://gitlab.com/DarrenEastman)              | Principal Product Manager, GitLab Runner | ☕️ |
| `2023-02-14` | 🇺🇸 [Alana Bellucci](https://gitlab.com/abellucci)                  | Senior Product Manager, Monitor:Respond | ☕️ |
| `2023-02-07` | 🇮🇪 [Fabio Pitino](https://gitlab.com/fabiopitino)                  | Staff Backend Engineer, Verify | ☕️ |
| `2023-02-02` | 🇩🇪 [Alexander Dess](https://gitlab.com/alex-dess)                  | Senior Solutions Architect | 🍩 |
| `2023-02-02` | 🇵🇱 [Grzegorz Bizon](https://gitlab.com/grzesiek)                   | Principal Engineer | ☕️ |
| `2023-02-02` | 🇩🇪 [Michelle Torres](https://gitlab.com/michelletorres)            | Engineering Manager, Package:Container Registry | ☕️ |
| `2023-01-26` | 🇺🇸 [Scott Hampton](https://gitlab.com/shampton)                    | Engineering Manager, Verify:Pipeline Insights | ☕️ |
| `2023-01-25` | 🇨🇦 [Caroline Simpson](https://gitlab.com/carolinesimpson)          | Backend Engineering Manager, Verify:Pipeline Execution | ☕️ |
| `2023-01-25` | 🇬🇧 [Catherine Pope](https://gitlab.com/drcatherinepope)            | Technical Writer | ☕️ |
| `2023-01-19` | 🇺🇸 [Giuliana Lucchesi](https://gitlab.com/glucchesi)               | Manager, People Business Partner | ☕️ |
| `2023-01-18` | 🇺🇸 [Carrie Kroutil](https://gitlab.com/ckroutil)                   | Fullstack Engineering Manager, Package:Package Registry | ☕️ |
| `2023-01-18` | 🇳🇱 [Vladimir Shushlin](https://gitlab.com/vshushlin)               | Acting Fullstack Engineering Manager, Release | ☕️ |
| `2023-01-17` | 🇨🇦 [Roger Woo](https://gitlab.com/rogerwoo)                        | Senior Product Manager | ☕️ |
| `2023-01-09` | 🇨🇦 [Cheryl Li](https://gitlab.com/cheryl.li)                       | Sr Engineering Manager, Verify | ☕️ |
| `2023-01-06` | 🇳🇱 [Ankit Bhatnagar](https://gitlab.com/ankitbhatnagar)            | Senior Backend Engineer, Monitor:Observability | ☕️ |
| `2023-01-06` | 🇨🇦 [Elliot Rushton](https://gitlab.com/erushton)                   | Engineering Manager, Verify:Runner | ☕️ |
| `2023-01-05` | 🇺🇸 [Jeff Green](https://gitlab.com/jgreen11)                       | Senior Customer Success Manager, Public Sector | 🍩 |
| `2022-12-20` | 🇩🇪 [Tatiana Fernandez](https://gitlab.com/tatianafernandez)        | Manager, Business Development Enterprise EMEA | 🍩 |
| `2022-12-15` | 🇪🇸 [Idir Ouhab](https://www.gitlab.com/iouhab)                     | Senior Solutions Architect | ☕️ |
| `2022-12-14` | 🇩🇪 [Michael Friedrich](https://gitlab.com/dnsmichi)                | Senior Developer Evangelist | ☕️ |
| `2022-12-13` | 🇺🇸 [Mat Appelman](https://gitlab.com/mappelman)                    | Principal Engineer, Monitor | ☕️ |
| `2022-12-08` | 🇮🇪 [Ian Murray](https://gitlab.com/ian.murray)                     | Senior Frontend Engineer, Monitor:Observability | ☕️ |
