<!-- TITLE: Monthly checklist - MONTH YEAR -->

## Engineering

- Monday: read [EWIR](https://docs.google.com/document/d/1JBdCl3MAOSdlgq3kzzRmtzTsFWsTIQ9iQg0RHhMht6E/edit#)
  - [ ] 1st Monday
  - [ ] 2nd Monday
  - [ ] 3rd Monday
  - [ ] 4th Monday
  - [ ] 5th Monday
- Monday: read [async updates from Ops teams](https://gitlab.com/gitlab-com/ops-sub-department/ops-status-updates/-/issues/?sort=created_date&state=all&first_page_size=100)
  - [ ] 1st Monday
  - [ ] 2nd Monday
  - [ ] 3rd Monday
  - [ ] 4th Monday
  - [ ] 5th Monday

## Release

- [ ] By the 17th: merge [our release posts](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests?scope=all&state=all&label_name[]=group%3A%3Arespond&label_name[]=release%20post)
- [ ] On the 19th: verify that [all our bug fixes](https://gitlab.com/groups/gitlab-org/-/issues/?sort=milestone_due_desc&state=closed&label_name%5B%5D=group%3A%3Arespond&label_name%5B%5D=type%3A%3Abug&milestone_title=Started&first_page_size=100) are included in [the upcoming release post](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests?scope=all&state=opened&label_name[]=release%20post&label_name[]=release)

/assign @francoisrose

