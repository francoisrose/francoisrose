## François's README

Hi 👋

This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before.

It’s also a well-intentioned effort at building some trust by being intentionally vulnerable, and to share my ideas of a good working relationship to reduce the anxiety of people who might be on my team.

Please feel free to invite me to a [coffee chat](https://gitlab.com/francoisrose/francoisrose/-/blob/main/coffee_chats.md)! You can also contribute to this page by opening a merge request.

## About me

- I live in Berlin, Germany with my wife and son.
- My family is originally from the [Bretagne](https://en.wikipedia.org/wiki/Brittany) region of France. I've lived in the Netherlands (born and raised there), France, Denmark, Ireland, and Germany.
- I enjoy playing volleyball, video games, watching series/movies, and traveling.
- At GitLab, I'm an Engineering Manager in the [Create:Code Review](https://handbook.gitlab.com/handbook/engineering/development/dev/create/code-review/) group. Before that, I worked on [Editor Extensions](https://docs.gitlab.com/ee/editor_extensions/), [Service Desk](https://docs.gitlab.com/ee/user/project/service_desk/) and [Observability](https://about.gitlab.com/direction/monitor/observability/).
- I've worked as a software engineer since 2011, in various fields: computer vision, food science, cryptanalysis, digital music software deployment, audio streaming.
- I became a manager in 2019 as a way to work with people directly, while keeping a foot in the tech world.
- I'm curious about the [Engineer/Manager pendulum](https://charity.wtf/2017/05/11/the-engineer-manager-pendulum/), and would rather leave the door back to the IC path open for now.

## GitLab values and operating principles

The [GitLab values](https://about.gitlab.com/handbook/values/) are a big reason I work here. The ones that resonate most with me are
[Transparency](https://about.gitlab.com/handbook/values/#transparency),
[Collaboration](https://about.gitlab.com/handbook/values/#collaboration),
and [Efficiency](https://about.gitlab.com/handbook/values/#efficiency).

The operating principles that stand out to me the most, in no particular order:
- [Public by default](https://about.gitlab.com/handbook/values/#public-by-default) and [Findability](https://about.gitlab.com/handbook/values/#findability) - an effective default to work remotely and establish trust
- [Family and friends first, work second](https://about.gitlab.com/handbook/values/#family-and-friends-first-work-second) - work-life balance is very important to me
- [Write things down](https://about.gitlab.com/handbook/values/#write-things-down) and [Bias towards asynchronous communication](https://about.gitlab.com/handbook/values/#bias-towards-asynchronous-communication) - this is not only more inclusive, but makes efficiency compound over time
- [It's impossible to know everything](https://about.gitlab.com/handbook/values/#its-impossible-to-know-everything) - that's why we work in teams and [Don't let each other fail](https://about.gitlab.com/handbook/values/#dont-let-each-other-fail)
- [Measure results not hours](https://about.gitlab.com/handbook/values/#measure-results-not-hours)
- [Kindness](https://about.gitlab.com/handbook/values/#kindness) and [Say thanks](https://about.gitlab.com/handbook/values/#say-thanks)
- and many more

## Communicating and working with me

**How to reach me**
- To get my attention on a GitLab issue/MR/etc, please mention me with @francoisrose. I may miss your message otherwise, even if I'm subscribed to the issue.
- If you're blocked by me or I'm too slow to respond, don't hesistate to ping me on Slack. I will be thankful for it!

**My working style**
- I appreciate both written and verbal communication. Talking helps me connect, writing serves to remember.
- I enjoy understanding the bigger picture and how everything fits together (or doesn’t). I prefer to go broad rather than too deep.
- I am very diplomatic by nature. This has benefits and drawbacks. I can serve as a good third party to a discussion, but can sometimes fail to challenge directly.
- I like structure. If something feels sub-optimal or just broken, I will naturally try to fix it.
- I approach my role as a highly mutable one. Think of a puzzle piece that takes whatever shape is needed to complete the picture and support the team.

**How to help me**
- I work best when I have an understanding of the context, even at a high-level. I ask questions when things are unclear. Help me understand, and my value will increase.
- The more direct you are with me, the easier it'll be for me.

**Favorite things to work on**
- Anything related to data or number crunching, and visualization of those.
- Talking to people, hearing them out, and seeing their progress over time.
- Optimizing processes and automating things.

**Least favorite things to work on**
- Tasks whose purpose I don't understand, or disagree with.
- Building long-term visions, mostly because I'm not great at it. I'm more comfortable identifying current constraints and planning short/mid term.
